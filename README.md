# Pattern Recognition

R-files for semester project in pattern recognition. In particular, the assignment is to try make a hypothesis on a given set of real-world dev-ops data ( from GitHub ) and eventually apply some unsupervised learning techniques ( preprocessing + clustering ) to extract some sort of inference of the coding behavioral differences among the developers, ops and devops.